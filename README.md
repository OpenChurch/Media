# Media

Offene und frei zugängliche Medien, die im oder für den kirchlichen Gebrauch erstellt wurden.

Bitte beachte die [README im Wiki](https://codeberg.org/OpenChurch/Wiki/src/branch/master/README.md).